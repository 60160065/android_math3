package buu.phatcharapol.calculator

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import buu.phatcharapol.calculator.databinding.FragmentMultiplyBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 * Use the [MultiplyFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MultiplyFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var binding: FragmentMultiplyBinding
    private lateinit var viewModel: MultiplyViewModel
    private lateinit var viewModelFactory: MultiplyViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_multiply,
            container,
            false
        )
        Log.i("MultiplyFragment", "Called ViewModelProvider.get")
        viewModelFactory = MultiplyViewModelFactory(
            lastCorrect = MultiplyFragmentArgs.fromBundle(requireArguments()).correctScore,
            lastIncorrect = MultiplyFragmentArgs.fromBundle(requireArguments()).incorrectScore
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(MultiplyViewModel::class.java)
        binding.apply {

            btn1.setOnClickListener {
                checkGame(btn1)
            }
            btn2.setOnClickListener {
                checkGame(btn2)
            }
            btn3.setOnClickListener {
                checkGame(btn3)
            }
        }

        binding.multiplyViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        gameStart()
        binding.btnExit.setOnClickListener {
            gameEndGoBack()
        }
        return binding.root
    }

    private fun gameStart() {
        binding.result.visibility = View.INVISIBLE
        setRandomButton()
    }

    private fun gameEndGoBack() {
        NavHostFragment.findNavController(this).navigate(MultiplyFragmentDirections.actionMultiplyFragmentToTitleFragment(
            viewModel.correct.value!!,
            viewModel.incorrect.value!!
        ))
    }

    private fun delayResetGame() {
        Handler().postDelayed({
            viewModel.startGame()
            gameStart()
        }, 1000)
    }

    private fun showCorrect() {
        binding.apply {
            result.visibility = View.VISIBLE
            result.text = "Correct!"
            result.setTextColor(Color.GREEN)
            trueScore.text = "Correct: " + viewModel.correct.value?.toString()
        }
    }

    private fun showIncorrect() {
        binding.apply {
            result.visibility = View.VISIBLE
            result.text = "Incorrect!"
            result.setTextColor(Color.RED)
            falseScore.text = "Incorrect: " + viewModel.incorrect.value?.toString()
        }
    }

    private fun checkGame(text: Button) {
        binding.apply {
            if (text.text == viewModel.ansNumber.value.toString()) {
                viewModel.scoreUp()
                showCorrect()
                delayResetGame()
            } else {
                viewModel.scoreDown()
                showIncorrect()
            }
        }
    }

    private fun setRandomButton() {
        binding.apply {
            btn1.text = viewModel.choice1.value.toString()
            btn2.text = viewModel.choice2.value.toString()
            btn3.text = viewModel.choice3.value.toString()
        }
    }

}