package buu.phatcharapol.calculator.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_table")
data class Users(
    @PrimaryKey(autoGenerate = true)
    var userId: Long = 0L,
    @ColumnInfo(name = "name")
    var username: Long = 0L,
    @ColumnInfo(name = "pass")
    var password: Long = 0L,
    @ColumnInfo(name = "correct")
    var correctScore: Int = 0,
    @ColumnInfo(name = "incorrect")
    var incorrectScore: Int = 0
)