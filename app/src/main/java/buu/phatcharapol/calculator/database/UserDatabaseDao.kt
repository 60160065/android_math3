package buu.phatcharapol.calculator.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface UserDatabaseDao {
    //INSERT ONE
    @Insert
    fun insert(user: Users)

    //UPDATE ONE
    @Update
    fun update(user: Users)

    //GET ALL
    @Query("SELECT * from user_table WHERE userId = :key")
    fun get(key: Long): Users?

    //DELETE ALL
    @Query("DELETE FROM user_table")
    fun clear()

    //GER CURRENT
    @Query("SELECT * FROM user_table ORDER BY userId DESC LIMIT 1")
    fun getCurrentUser(): Users?

    //GER ALL DESC
    @Query("SELECT * FROM user_table ORDER BY userId DESC")
    fun getAllUsers(): LiveData<List<Users>>
}